#!/bin/bash

make clean
make

#ALGNUM=$1

LOGFOLDER=output_logs_wavefront
LOGFILE=output_logs_wavefront/log_ALL.txt

echo "Steven Schmidt" > $LOGFILE
echo "CS6230 HW4 Part A" >> $LOGFILE
echo "" >> $LOGFILE

for ALGNUM in 0 1 2 3
do
    for GRID in 10 20 40 80 160 320
    do
        for NUMTHR in 1 2 4 8 16 24
        do
            ./out $GRID $GRID 0.25 1.0 8e-8 20000 $NUMTHR $ALGNUM  >> $LOGFILE
    
            echo "" >> $LOGFILE
            echo "" >> $LOGFILE
            echo "********************************" >> $LOGFILE
            echo "" >> $LOGFILE
            echo "" >> $LOGFILE
        done
    done
done

cat $LOGFILE | grep "openmp_run_results" | sort > $LOGFOLDER/grep_output.txt
cat $LOGFILE | grep "openmp_run_formatted_results" | sort | cut -c 57- > $LOGFOLDER/grep_formatted_output.txt







