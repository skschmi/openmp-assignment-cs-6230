import numpy
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot

time          = 0
numproc       = 1 
numthreads    = 2  
m             = 3
n             = 4
alpha         = 5
omega         = 6
tol           = 7
it_max        = 8
iters_reached = 9
jacobi_alg    = 10

data = kingspeak_data = numpy.loadtxt("output_logs_wavefront2/grep_formatted_output.txt")

algorithms = [0, 1, 2, 3]
meshsizes = [10,20,40,80,160,320]
plotformats = ['b-o','c-o','g-o','k-o','y-o','r-o']

##########################################
## Speedup - each plot an algorithm type
##########################################

for algnum in algorithms:

    pyplot.figure()
    index = 0
    for mshsz in meshsizes:
        alg = data[numpy.where(data[:,jacobi_alg]==algnum)[0],:]
        this_meshsize_data = alg[numpy.where(alg[:,m]==mshsz)[0],:]
    
        speedup = this_meshsize_data[0,time]/this_meshsize_data[:,time]
        
        pyplot.plot(this_meshsize_data[:,numthreads],speedup,plotformats[index])
        index += 1
    
    pyplot.title("Speedup:\nAlgorithm Number: "+str(algnum))
    pyplot.xlabel("Number of Threads (One Thread Per Core)")
    pyplot.ylabel("Speedup")
    pyplot.legend(["10x10","20x20","40x40","80x80","160x160","320x320"],loc='upper left', shadow=True)
    pyplot.ylim([0, 12])

    pyplot.savefig("figures/fig_speedup_alg_"+str(algnum).zfill(4)+".pdf")


#####################################
## Speedup - each plot a mesh size
#####################################

for mshsz in meshsizes:

    pyplot.figure()
    index = 0
    for algnum in algorithms:
        this_meshsize_data = data[numpy.where(data[:,m]==mshsz)[0],:]
        alg = this_meshsize_data[numpy.where(this_meshsize_data[:,jacobi_alg]==algnum)[0],:]
        
        speedup = alg[0,time]/alg[:,time]
        
        pyplot.plot(alg[:,numthreads],speedup,plotformats[index])
        index += 1
    
    pyplot.title("Speedup:\nMesh Size: "+str(mshsz)+"x"+str(mshsz))
    pyplot.xlabel("Number of Threads (One Thread Per Core)")
    pyplot.ylabel("Speedup")
    pyplot.legend(["Algorithm 0","Algorithm 1","Algorithm 2","Algorithm 3"],loc='upper left', shadow=True)
    pyplot.ylim([0, 12])

    pyplot.savefig("figures/fig_speedup_mshsz_"+str(mshsz)+"x"+str(mshsz)+".pdf")


##########################
## Time to Completion
##########################

for mshsz in meshsizes:

    pyplot.figure()
    index = 0
    for algnum in algorithms:
        this_meshsize_data = data[numpy.where(data[:,m]==mshsz)[0],:]
        alg = this_meshsize_data[numpy.where(this_meshsize_data[:,jacobi_alg]==algnum)[0],:]
        
        pyplot.plot(alg[:,numthreads],alg[:,time],plotformats[index])
        index += 1
    
    pyplot.title("Time to Completion (sec):\nMesh Size: "+str(mshsz)+"x"+str(mshsz))
    pyplot.xlabel("Number of Threads (One Thread Per Core)")
    pyplot.ylabel("Time To Completion (seconds)")
    pyplot.legend(["Algorithm 0","Algorithm 1","Algorithm 2","Algorithm 3"],loc='upper right', shadow=True)

    pyplot.savefig("figures/fig_times_mshsz_"+str(mshsz)+"x"+str(mshsz)+".pdf")



##########################
## Karp-Flatt Matric Plot
##########################

for mshsz in meshsizes:

    pyplot.figure()
    index = 0
    ymaxval = 0;
    for algnum in algorithms:
        this_meshsize_data = data[numpy.where(data[:,m]==mshsz)[0],:]
        alg = this_meshsize_data[numpy.where(this_meshsize_data[:,jacobi_alg]==algnum)[0],:]
        
        speedup = alg[0,time]/alg[:,time]
        karpflatt = numpy.zeros(speedup.shape)
        karpflatt[1:] = ((1./speedup[1:]) - (1./alg[1:,numthreads])) / (1 - (1./alg[1:,numthreads]))
        
        ymaxval = numpy.maximum(ymaxval,karpflatt.max())
        
        pyplot.plot(alg[:,numthreads],karpflatt,plotformats[index])
        index += 1
    
    pyplot.title("Karp-Flatt Metric (Including Gauss-Seidel)\nMesh Size: "+str(mshsz)+"x"+str(mshsz))
    pyplot.xlabel("Number of Threads (One Thread Per Core)")
    pyplot.ylabel("Karp-Flatt $f$")
    pyplot.legend(["Algorithm 0","Algorithm 1","Algorithm 2","Algorithm 3"],loc='upper left', shadow=True)
    pyplot.ylim([-0.1*ymaxval, 1.5*ymaxval])

    pyplot.savefig("figures/fig_karpflatt_mshsz_"+str(mshsz)+"x"+str(mshsz)+".pdf")


for mshsz in meshsizes:

    pyplot.figure()
    index = 0
    ymaxval = 0;
    for algnum in algorithms[0:-1]:
        this_meshsize_data = data[numpy.where(data[:,m]==mshsz)[0],:]
        alg = this_meshsize_data[numpy.where(this_meshsize_data[:,jacobi_alg]==algnum)[0],:]
        
        speedup = alg[0,time]/alg[:,time]
        karpflatt = numpy.zeros(speedup.shape)
        karpflatt[1:] = ((1./speedup[1:]) - (1./alg[1:,numthreads])) / (1 - (1./alg[1:,numthreads]))
        
        ymaxval = numpy.maximum(ymaxval,karpflatt.max())
        
        pyplot.plot(alg[:,numthreads],karpflatt,plotformats[index])
        index += 1
    
    pyplot.title("Karp-Flatt Metric (Only Jacobi Algs):\nMesh Size: "+str(mshsz)+"x"+str(mshsz))
    pyplot.xlabel("Number of Threads (One Thread Per Core)")
    pyplot.ylabel("Karp-Flatt $f$")
    pyplot.legend(["Algorithm 0","Algorithm 1","Algorithm 2","Algorithm 3"],loc='upper left', shadow=True)
    pyplot.ylim([-0.1*ymaxval, 1.5*ymaxval])

    pyplot.savefig("figures/fig_karpflatt_withoutGS_mshsz_"+str(mshsz)+"x"+str(mshsz)+".pdf")



