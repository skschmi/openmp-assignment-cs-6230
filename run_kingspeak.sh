#!/bin/bash
#PBS -l nodes=1:ppn=32,walltime=1:00:00
#PBS -A CS6230
#PBS -N openmp_hw4
#PBS -M schmidticus@gmail.com

cd /uufs/chpc.utah.edu/common/home/u0353569/cs6230/HW4/
#source /uufs/telluride.arches/sys/pkg/openmpi/std/etc/openmpi.sh

make clean
make

LOGFILE=output_logs/log_ALL.txt

echo "Steven Schmidt" > $LOGFILE
echo "CS6230 HW4 Part A" >> $LOGFILE
echo "" >> $LOGFILE

for ALGNUM in 0 1 2
do
    for GRID in 10 20 40 80 160 320
    do
        for NUMTHR in 1 2 4 8 16 24 32
        do
            ./out $GRID $GRID 0.25 1.0 8e-8 20000 $NUMTHR $ALGNUM  >> $LOGFILE
    
            echo "" >> $LOGFILE
            echo "" >> $LOGFILE
            echo "********************************" >> $LOGFILE
            echo "" >> $LOGFILE
            echo "" >> $LOGFILE
        done
    done
done

cat $LOGFILE | grep "openmp_run_results" | sort > output_logs/grep_output.txt
cat $LOGFILE | grep "openmp_run_formatted_results" | sort | cut -c 57- > output_logs/grep_formatted_output.txt







