#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef _OPENMP
    #include <omp.h>
#endif

#include <time.h>
#include <sys/time.h>

#ifdef __MACH__
    #include <mach/clock.h>
    #include <mach/mach.h>
#endif

int main( int argc, char *argv[] );
void driver( int m, int n, double alpha, double omega, double tol, int it_max, int jacobi_alg, int *iters_reached );
void error_check( int m, int n, double alpha, double u[], double f[] );
void jacobi( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached );
void jacobi_alternate1( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached );
void jacobi_alternate2( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached );
void gauss_seidel_number0( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached );
double *rhs_set( int m, int n, double alpha );
void getWallTime(struct timespec *ts);
struct timespec diffBetweenStartAndEnd(struct timespec *start, struct timespec *end);
void timestamp( void );
double u_exact( double x, double y );
double uxx_exact( double x, double y );
double uyy_exact( double x, double y );



/******************************************************************************/
/******************************************************************************/
/*
Purpose:

MAIN is the main program for HELMHOLTZ.

Discussion:

HELMHOLTZ solves a discretized Helmholtz equation.

The two dimensional region given is:

-1 <= X <= +1
-1 <= Y <= +1

The region is discretized by a set of M by N nodes:

P(I,J) = ( X(I), Y(J) )

where, for 0 <= I <= M-1, 0 <= J <= N - 1, (C/C++ convention)

X(I) = ( 2 * I - M + 1 ) / ( M - 1 )
Y(J) = ( 2 * J - N + 1 ) / ( N - 1 )

The Helmholtz equation for the scalar function U(X,Y) is

- Uxx(X,Y) -Uyy(X,Y) + ALPHA * U(X,Y) = F(X,Y)

where ALPHA is a positive constant.  We suppose that Dirichlet
boundary conditions are specified, that is, that the value of
U(X,Y) is given for all points along the boundary.

We suppose that the right hand side function F(X,Y) is specified in 
such a way that the exact solution is

U(X,Y) = ( 1 - X**2 ) * ( 1 - Y**2 )

Using standard finite difference techniques, the second derivatives
of U can be approximated by linear combinations of the values
of U at neighboring points.  Using this fact, the discretized
differential equation becomes a set of linear equations of the form:

A * U = F

These linear equations are then solved using a form of the Jacobi 
iterative method with a relaxation factor.

Directives are used in this code to achieve parallelism.
All do loops are parallized with default 'static' scheduling.

Modified:

14 December 2007

Author:

Joseph Robicheaux, 
Sanjiv Shah,
Kuck and Associates, Inc. (KAI)

C translation by John Burkardt
*/
/******************************************************************************/
/******************************************************************************/
int main( int argc, char *argv[] )
{
    int m = 5;
    int n = 5;
    double alpha = 0.25;
    double omega = 1.1;
    double tol = 0.0001;
    int it_max = 20;

    struct timespec start;
    struct timespec end;
    
    int numproc = 1;
    int numthreads = 1;
    
    int jacobi_alg = 0;

    if( argc == 1 ) {
        //We're good.
    }
    else if( argc == 3 ) {
        m = atoi(argv[1]);
        n = atoi(argv[2]);
    }
    else if( argc == 9 ) {
        m = atoi(argv[1]);
        n = atoi(argv[2]);
        sscanf(argv[3], "%lf", &alpha);
        sscanf(argv[4], "%lf", &omega);
        sscanf(argv[5], "%lf", &tol);
        it_max = atoi(argv[6]);
        numthreads = atoi(argv[7]);
        jacobi_alg = atoi(argv[8]);
    }
    else {
        printf( "Default args: m=%d, n=%d, alpha=%f, omega=%f, tol=%e, it_max=%d, numthreads=%d, jacobi_alg=%d\n\n",
                        m,n,alpha,omega,tol,it_max,numthreads,jacobi_alg);
        return 0;
    }

    printf("***********************************************\n");
    
    printf( "\n" );
    printf( "HELMHOLTZ\n" );
    printf( "  C version\n" );
    printf( "\n" );
    printf( "  A program which solves the 2D Helmholtz equation.\n" );
    printf( "\n" );
    printf( "  This program includes Open MP directives, so that\n" );
    printf( "  it can be run sequentially, or in parallel.\n" );

    #ifdef _OPENMP
        printf( "\n" );
        printf( "  This program is being run in parallel.\n" );
        printf( "\n" );
        printf( "  The number of processors available:\n" );
        numproc = omp_get_num_procs();
        printf( "  OMP_GET_NUM_PROCS () = %d\n", numproc );
        printf( "\n" );
        //printf( "  The number of threads available:\n" );
        //printf( "  OMP_GET_NUM_THREADS () = %d\n", numthreads );

        //Set the number of threads (passed in the parameter list)
        omp_set_num_threads(numthreads);
        
        printf("  The number of threads we will use: %d\n",numthreads); 
        #pragma omp parallel
        {
            // Code inside this region runs in parallel.
            printf("  Hello from thread %d!\n",omp_get_thread_num());
        }
        
    #endif
    
    printf( "\n" );
    printf( "  The region is [-1,1] x [-1,1].\n" );
    printf( "  The number of nodes in the X direction is M = %d\n", m );
    printf( "  The number of nodes in the Y direction is N = %d\n", n );
    printf( "  Number of variables in linear system M * N  = %d\n", m * n );
    printf( "  The scalar coefficient in the Helmholtz equation is ALPHA = %f\n", alpha );
    printf( "  The relaxation value is OMEGA = %f\n", omega );
    printf( "  The error tolerance is TOL = %e\n", tol );
    printf( "  The maximum number of Jacobi iterations is IT_MAX = %d\n", it_max );
    
    int iters_reached;
    
    //timestamp();
    getWallTime(&start);
    
    //Call the driver routine.
    driver(m, n, alpha, omega, tol, it_max, jacobi_alg, &iters_reached);
    
    //timestamp();
    getWallTime(&end);

    printf( "\n" );
    printf( "HELMHOLTZ\n" );
    printf( "  Normal end of execution.\n" );
    printf( "\n" );
 
    struct timespec compute_time = diffBetweenStartAndEnd(&start, &end);
    printf("openmp_run_results %03da-%03dm-%03dn-%03dp-%03dth: compute_time=%lld.%.9ld s  numproc=%d numthreads=%d m=%d n=%d alpha=%2.14f omega=%2.14f tol=%2.4e it_max=%d iters_reached=%d jacobi_alg=%d\n",
                jacobi_alg,m,n,numproc,numthreads,(long long)compute_time.tv_sec,compute_time.tv_nsec,numproc,numthreads,m,n,alpha,omega,tol,it_max,iters_reached,jacobi_alg);
    printf("\n");
    printf("openmp_run_formatted_results %03da-%03dm-%03dn-%03dp-%03dth: %lld.%.9ld %d %d %d %d %2.14f %2.14f %2.4e %d %d %d\n",
                jacobi_alg,m,n,numproc,numthreads,(long long)compute_time.tv_sec,compute_time.tv_nsec,numproc,numthreads,m,n,alpha,omega,tol,it_max,iters_reached,jacobi_alg);
    printf("\n");
    
    
    printf("***********************************************\n");
    return 0;
}






/******************************************************************************/
/******************************************************************************/
/*
Purpose:

DRIVER allocates arrays and solves the problem.

Modified:

21 November 2007

Author:

Joseph Robicheaux, 
Sanjiv Shah,
Kuck and Associates, Inc. (KAI)

C++ translation by John Burkardt

Parameters:

Input, int M, N, the number of grid points in the 
X and Y directions.

Input, int IT_MAX, the maximum number of Jacobi 
iterations allowed.

Input, double ALPHA, the scalar coefficient in the
Helmholtz equation.

Input, double OMEGA, the relaxation parameter, which
should be strictly between 0 and 2.  For a pure Jacobi method,
use OMEGA = 1.

Input, double TOL, an error tolerance for the linear
equation solver.
*/
/******************************************************************************/
/******************************************************************************/
void driver( int m, int n, double alpha, double omega, double tol, int it_max, int jacobi_alg, int *iters_reached )
{
    double *f;
    int i;
    int j;
    double *u;

    //Initialize the data.

    f = rhs_set(m, n, alpha);

    u = (double *) malloc(m*n*sizeof(double));

    # pragma omp parallel for private(i,j) shared(n,m,u)
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            u[i+j*m] = 0.0;
        }
    }

    //Solve the Helmholtz equation.
    if(jacobi_alg == 0) {
        jacobi( m, n, alpha, omega, u, f, tol, it_max, iters_reached );
    }
    else if(jacobi_alg == 1) {
        jacobi_alternate1( m, n, alpha, omega, u, f, tol, it_max, iters_reached );
    }
    else if(jacobi_alg == 2) {
        jacobi_alternate2( m, n, alpha, omega, u, f, tol, it_max, iters_reached );
    }
    else if(jacobi_alg == 3) {
        gauss_seidel_number0( m, n, alpha, omega, u, f, tol, it_max, iters_reached );
    }
    
    //Determine the error.
    error_check( m, n, alpha, u, f );

    free(f);
    free(u);

    return;
}


/******************************************************************************/
/******************************************************************************/
/*
Purpose:

ERROR_CHECK determines the error in the numerical solution.

Modified:

21 November 2007

Author:

Joseph Robicheaux, 
Sanjiv Shah,
Kuck and Associates, Inc. (KAI)

C translation by John Burkardt

Parameters:

Input, int M, N, the number of grid points in the 
X and Y directions.

Input, double ALPHA, the scalar coefficient in the
Helmholtz equation.  ALPHA should be positive.

Input, double U[M*N], the solution of the Helmholtz equation 
at the grid points.

Input, double F[M*N], values of the right hand side function 
for the Helmholtz equation at the grid points.
*/
/******************************************************************************/
/******************************************************************************/
void error_check( int m, int n, double alpha, double u[], double f[] )
{
    double error_norm;
    int i;
    int j;
    double u_norm;
    double u_true;
    double u_true_norm;
    double x;
    double y;


    u_norm = 0.0;
    # pragma omp parallel for private(i,j) shared( n, m, u ) reduction( + : u_norm )
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            u_norm = u_norm + u[i+j*m] * u[i+j*m];
        }
    }
    u_norm = sqrt(u_norm);


    u_true_norm = 0.0;
    error_norm = 0.0;
    # pragma omp parallel for private( i, j, x, y, u_true ) shared(n,m) reduction( + : error_norm, u_true_norm)
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            x =(double)( 2 * i - m + 1 ) / (double)( m - 1 );
            y =(double)( 2 * j - n + 1 ) / (double)( n - 1 );
            u_true = u_exact( x, y );
            error_norm = error_norm + (u[i+j*m] - u_true) * (u[i+j*m] - u_true);
            u_true_norm = u_true_norm + u_true * u_true;
        }
    }

    error_norm = sqrt( error_norm );
    u_true_norm = sqrt( u_true_norm );

    printf( "\n" );
    printf( "  Computed U L2 norm :       %f\n", u_norm );
    printf( "  Computed U_EXACT L2 norm : %f\n", u_true_norm );
    printf( "  Error L2 norm:             %f\n", error_norm );

    return;
}








/******************************************************************************/
/******************************************************************************/
/*
JACOBI alternate number 2: Combining for-loops to avoid overhead.
*/
/******************************************************************************/
/******************************************************************************/
void jacobi_alternate2( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached )
{
    double ax;
    double ay;
    double b;
    double dx;
    double dy;
    double error;
    int i;
    int it;
    int j;
    
    int itcount = 0;
    
    double *u_old;
    
    double first_part;
    
    double error_norm;
    double error_norm_old;
    int num_old_errors = 0;

    //Initialize the coefficients.

    dx = 2.0 /(double)( m - 1 );
    dy = 2.0 /(double)( n - 1 );

    ax = - 1.0 / dx / dx;
    ay = - 1.0 / dy / dy;
    b  = + 2.0 / dx / dx + 2.0 / dy / dy + alpha;

    u_old = (double *) malloc( m * n * sizeof(double) );

    //***********************
    //***********************
    //***********************


    int this_thread;
    int num_threads;
    double error_norm_local;
    
    //***********************
    //***********************
    //***********************

    for( it = 1; it <= it_max; it++ ) {

        error_norm_old = error_norm;
        error_norm = 0.0;
        itcount++;

#ifdef _OPENMP

        # pragma omp parallel private(this_thread,num_threads,error_norm_local,i,j,error,first_part) shared(m,n,ax,ay,b,f,omega,u,u_old) reduction( + : error_norm )
        {
            this_thread = omp_get_thread_num();
            num_threads = omp_get_num_threads();
            error_norm_local = 0.0;
        
            //Copy new solution into old.            
            for(j = this_thread; j < n; j=j+num_threads) {
                for( i = 0; i < m; i++ ) {
                    u_old[i+m*j] = u[i+m*j];
                }
            }

            #pragma omp barrier

            //Compute stencil, residual, and update.
            for(j = this_thread; j < n; j=j+num_threads) {
                for( i = 0; i < m; i++ ) {
                
                    //Evaluate the residual.
                    if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                        error = u_old[i+j*m] - f[i+j*m];
                    }
                    else {
                        first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                                   + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                                   + b  *  u_old[i+j*m]     - f[i+j*m] );
                        error = first_part / b;
                    }

                    //Update the solution.
                    u[i+j*m] = u_old[i+j*m] - omega * error;

                    //Accumulate the residual error.
                    error_norm_local = error_norm_local + (error*error);
                }
            }
            error_norm += error_norm_local;
            
        }
    
#else

        //Copy new solution into old.
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                u_old[i+m*j] = u[i+m*j];
            }
        }

        //Compute stencil, residual, and update.
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u_old[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                               + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                               + b  *  u_old[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                    
                    //if(it == 1 && i == 4 && j == 4)
                    //    printf("first_part = %2.14f, b = %2.14f, error = %2.14f\n",first_part, b, error);
                }

                //Update the solution.
                u[i+j*m] = u_old[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm = error_norm + (error*error);
                
            }
        }

#endif
        //Error check.
        error_norm = sqrt(error_norm) / (double)(m*n);
        if( num_old_errors < 2 ) num_old_errors++;

        //Break or print
        if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            break;
        }
        else if(it % 100 == 0) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        }
    }
    
    //***********************
    //***********************
    //***********************

    (*iters_reached) = itcount;

    printf("b = %2.14f\n", b);
    printf( "\n" );
    printf( "  Total number of iterations %d\n", itcount );

    free(u_old);

    return;
}













/******************************************************************************/
/******************************************************************************/
/*
JACOBI alternate number 1: Combining for-loops to avoid overhead.
*/
/******************************************************************************/
/******************************************************************************/
void jacobi_alternate1( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached )
{
    double ax;
    double ay;
    double b;
    double dx;
    double dy;
    double error;
    int i;
    int it;
    int j;
    
    int itcount = 0;
    
    double *u_old;
    
    double first_part;
    
    double error_norm;
    double error_norm_old;
    int num_old_errors = 0;

    //Initialize the coefficients.

    dx = 2.0 /(double)( m - 1 );
    dy = 2.0 /(double)( n - 1 );

    ax = - 1.0 / dx / dx;
    ay = - 1.0 / dy / dy;
    b  = + 2.0 / dx / dx + 2.0 / dy / dy + alpha;

    u_old = (double *) malloc( m * n * sizeof(double) );

    //***********************
    //***********************
    //***********************

#ifdef _OPENMP

    int this_thread;
    int num_threads;
    double error_norm_local;

    //FOR TOMORROW: TODO: Move the OpenMP pragma down to only combine the two for-loop sections to see what happens.

    #pragma omp parallel private(it,i,j,this_thread,num_threads,first_part,error,error_norm_local) shared(itcount,error_norm,f,n,m,u,u_old,ax,ay,b,dx,dy,it_max,error_norm_old,num_old_errors)
    {
        this_thread = omp_get_thread_num();
        num_threads = omp_get_num_threads();
    
        //Timestep loop
        for( it = 1; it <= it_max; it++ ) {
        
            #pragma omp barrier
        
            error_norm_local = 0.0;
            
            if(this_thread == 0) {
                error_norm_old = error_norm;
                error_norm = 0.0;
                itcount++;
            }
        
            //Copy new solution into old.
            for(j = this_thread; j < n; j=j+num_threads) {
                for( i = 0; i < m; i++ ) {
                    u_old[i+m*j] = u[i+m*j];
                }
            }
            
            #pragma omp barrier
            
            //Compute stencil
            for(j = this_thread; j < n; j=j+num_threads) {
                for( i = 0; i < m; i++ ) {
                
                    //Evaluate the residual.
                    if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                        error = u_old[i+j*m] - f[i+j*m];
                    }
                    else {
                        first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                                   + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                                   + b  *  u_old[i+j*m]     - f[i+j*m] );
                        error = first_part / b;
                    }

                    //Update the solution.
                    u[i+j*m] = u_old[i+j*m] - omega * error;

                    //Accumulate the residual error.
                    error_norm_local += (error*error);

                }
            }
        
            #pragma omp barrier
        
            #pragma omp critical
            error_norm += error_norm_local;
            
            #pragma omp barrier
            
            if(this_thread == 0) {

                //Error check.
                error_norm = sqrt(error_norm) / (double)(m*n);
                if( num_old_errors < 2 ) num_old_errors++;

                if(it % 100 == 0) {
                    printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
                }
            }
            
            #pragma omp barrier
            
            //Break
            if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
                if(this_thread == 0) printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
                break;
            }
            
        }
    }
#else

    //***********************
    //***********************
    //***********************

    for( it = 1; it <= it_max; it++ ) {

        error_norm_old = error_norm;
        error_norm = 0.0;
        itcount++;

        //Copy new solution into old.
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                u_old[i+m*j] = u[i+m*j];
            }
        }

        //Compute stencil, residual, and update.
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u_old[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                               + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                               + b  *  u_old[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                    
                    //if(it == 1 && i == 4 && j == 4)
                    //    printf("first_part = %2.14f, b = %2.14f, error = %2.14f\n",first_part, b, error);
                }

                //Update the solution.
                u[i+j*m] = u_old[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm = error_norm + (error*error);
                
            }
        }
    
        //Error check.
        error_norm = sqrt(error_norm) / (double)(m*n);
        if( num_old_errors < 2 ) num_old_errors++;

        //Break or print
        if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            break;
        }
        else if(it % 100 == 0) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        }
    }
    
    //***********************
    //***********************
    //***********************
#endif

    (*iters_reached) = itcount;

    printf("b = %2.14f\n", b);
    printf( "\n" );
    printf( "  Total number of iterations %d\n", itcount );

    free(u_old);

    return;
}



/******************************************************************************/
/******************************************************************************/
/*
Purpose:

JACOBI applies the Jacobi iterative method to solve the linear system.

Modified:

21 November 2007

Author:

Joseph Robicheaux, 
Sanjiv Shah,
Kuck and Associates, Inc. (KAI)

C translation by John Burkardt

Parameters:

Input, int M, N, the number of grid points in the 
X and Y directions.

Input, double ALPHA, the scalar coefficient in the
Helmholtz equation.  ALPHA should be positive.

Input, double OMEGA, the relaxation parameter, which
should be strictly between 0 and 2.  For a pure Jacobi method,
use OMEGA = 1.

Input/output, double U(M,N), the solution of the Helmholtz
equation at the grid points.

Input, double F(M,N), values of the right hand side function 
for the Helmholtz equation at the grid points.

Input, double TOL, an error tolerance for the linear
equation solver.

Input, int IT_MAX, the maximum number of Jacobi 
iterations allowed.
*/
/******************************************************************************/
/******************************************************************************/
void jacobi( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached )
{
    double ax;
    double ay;
    double b;
    double dx;
    double dy;
    double error;
    int i;
    int it;
    int j;
    double *u_old;
    
    double first_part;
    
    double error_norm;
    double error_norm_old;
    int num_old_errors = 0;

    //Initialize the coefficients.

    dx = 2.0 /(double)( m - 1 );
    dy = 2.0 /(double)( n - 1 );

    ax = - 1.0 / dx / dx;
    ay = - 1.0 / dy / dy;
    b  = + 2.0 / dx / dx + 2.0 / dy / dy + alpha;

    u_old = (double *) malloc( m * n * sizeof(double) );

    for( it = 1; it <= it_max; it++ ) {
    
        error_norm_old = error_norm;
        error_norm = 0.0;

        //Copy new solution into old.
        # pragma omp parallel for private(i,j) shared(n,m,u,u_old)
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                u_old[i+m*j] = u[i+m*j];
            }
        }

        //Compute stencil, residual, and update.
        # pragma omp parallel for private(i,j,error,first_part) shared( m, n, ax, ay, b, f, omega, u, u_old ) reduction( + : error_norm )
        for( j = 0; j < n; j++ ) {
            for( i = 0; i < m; i++ ) {
                
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u_old[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                               + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                               + b  *  u_old[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                }

                //Update the solution.
                u[i+j*m] = u_old[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm = error_norm + (error*error);
                
            }
        }
    
        //Error check.
        error_norm = sqrt(error_norm) / (double)(m*n);
        if( num_old_errors < 2 ) num_old_errors++;

        //Break or print
        if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            break;
        }
        else if(it % 100 == 0) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        }

    }
    
    (*iters_reached) = it;
    
    printf("b = %2.14f\n", b);
    printf( "\n" );
    printf( "  Total number of iterations %d\n", it );

    free(u_old);

    return;
}


















/******************************************************************************/
/******************************************************************************/
/*

Gauss Seidel

NOTE: This assumes m and n are the same length

*/
/******************************************************************************/
/******************************************************************************/
void gauss_seidel_number0( int m, int n, double alpha, double omega, double u[], double f[], double tol, int it_max, int *iters_reached )
{
    double ax;
    double ay;
    double b;
    double dx;
    double dy;
    double error;
    int i;
    int it;
    int j;
    int k;
    double *u_old;
    
    double first_part;
    
    double error_norm;
    double error_norm_old;
    int num_old_errors = 0;
    
    if( m != n ) {
        for(i = 0; i < 100; i++) {
            printf("ERROR! m must equal n, and they are not equal!!\n");
        }
        return;
    }

    //Initialize the coefficients.

    dx = 2.0 /(double)( m - 1 );
    dy = 2.0 /(double)( n - 1 );

    ax = - 1.0 / dx / dx;
    ay = - 1.0 / dy / dy;
    b  = + 2.0 / dx / dx + 2.0 / dy / dy + alpha;

    int this_thread;
    int num_threads;
    int width;
    int diag;

    for( it = 1; it <= it_max; it++ ) {
    
        error_norm_old = error_norm;
        error_norm = 0.0;

        //Compute stencil, residual, and update.
        # pragma omp parallel private(i,j,k,width,error,first_part,this_thread,num_threads,diag) shared( m, n, ax, ay, b, f, omega, u ) reduction( + : error_norm )
        {
            this_thread = omp_get_thread_num();
            num_threads = omp_get_num_threads();
        
            width = 0;
            
            for( k = 0; k < (2*m)-1; k++ ) {            
                
                //Set the width of the wavefront
                if(k < m) {
                    width++;
                }
                else {
                    width--;
                }
                
                for(diag = this_thread; diag < width; diag = diag + num_threads) {
                
                    i = diag;
                    j = width - diag - 1;
                
                    //Evaluate the residual.
                    if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                        error = u[i+j*m] - f[i+j*m];
                    }
                    else {
                        first_part = (    ax * (u[i-1+j*m]   + u[i+1+j*m]) 
                                   + ay * (u[i+(j-1)*m] + u[i+(j+1)*m]) 
                                   + b  *  u[i+j*m]     - f[i+j*m] );
                        error = first_part / b;
                    }

                    //Update the solution.
                    u[i+j*m] = u[i+j*m] - omega * error;

                    //Accumulate the residual error.
                    error_norm = error_norm + (error*error);
                
                }
                
                #pragma omp barrier

            }
        
        }
    
        //Error check.
        error_norm = sqrt(error_norm) / (double)(m*n);
        if( num_old_errors < 2 ) num_old_errors++;

        //Break or print
        if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            break;
        }
        else if(it % 100 == 0) {
            printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        }

    }
    
    (*iters_reached) = it;
    
    printf("b = %2.14f\n", b);
    printf( "\n" );
    printf( "  Total number of iterations %d\n", it );

    free(u_old);

    return;
}
















/******************************************************************************/
/******************************************************************************/
/*
Purpose:

RHS_SET sets the right hand side F(X,Y).

Discussion:

The routine assumes that the exact solution and its second
derivatives are given by the routine EXACT.

The appropriate Dirichlet boundary conditions are determined
by getting the value of U returned by EXACT.

The appropriate right hand side function is determined by
having EXACT return the values of U, UXX and UYY, and setting

F = -UXX - UYY + ALPHA * U

Modified:

21 November 2007

Author:

Joseph Robicheaux, 
Sanjiv Shah,
Kuck and Associates, Inc. (KAI)

C translation by John Burkardt

Parameters:

Input, int M, N, the number of grid points in the 
X and Y directions.

Input, double ALPHA, the scalar coefficient in the
Helmholtz equation.  ALPHA should be positive.

Output, double RHS[M*N], values of the right hand side function 
for the Helmholtz equation at the grid points.
*/
/******************************************************************************/
/******************************************************************************/
double *rhs_set( int m, int n, double alpha )
{
    double *f;
    double f_norm;
    int i;
    int j;
    double x;
    double y;

    f = (double *) malloc( m * n * sizeof(double) );

    # pragma omp parallel for private(i,j) shared(f,n,m)
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            f[i+j*m] = 0.0;
        }
    }

    //Set the boundary conditions.
    j = 0;
    y = (double)( 2 * j - n + 1 ) / (double)( n - 1 );

    # pragma omp parallel for private(i,x) shared(f,m,j,y)
    for( i = 0; i < m; i++ ) {
        x = (double)( 2 * i - m + 1 ) /(double)( m - 1 );
        f[i+j*m] = u_exact( x, y );
    }

    j = n - 1;
    y = (double)( 2 * j - n + 1 ) / (double)( n - 1 );

    # pragma omp parallel for private(i,x) shared(f,m,j,y)
    for( i = 0; i < m; i++ ) {
        x = (double)( 2 * i - m + 1 ) / (double)( m - 1 );
        f[i+j*m] = u_exact( x, y );
    }

    i = 0;
    x = (double)( 2 * i - m + 1 ) / (double)( m - 1 );

    # pragma omp parallel for private(j,y) shared(f,m,n,i,x)
    for( j = 0; j < n; j++ ) {
        y = (double)( 2 * j - n + 1 ) / (double)( n - 1 );
        f[i+j*m] = u_exact( x, y );
    }

    i = m - 1;
    x = (double)( 2 * i - m + 1 ) / (double)( m - 1 );

    # pragma omp parallel for private(j,y) shared(f,m,n,i,x)
    for( j = 0; j < n; j++ ) {
        y = (double)( 2 * j - n + 1 ) / (double)( n - 1 );
        f[i+j*m] = u_exact( x, y );
    }
    
    //Set the right hand side F.
    # pragma omp parallel for private(i,j,x,y) shared(n,m,alpha,f)
    for( j = 1; j < n - 1; j++ ) {
        for( i = 1; i < m - 1; i++ ) {
            x = (double)( 2 * i - m + 1 ) / (double)( m - 1 );
            y = (double)( 2 * j - n + 1 ) / (double)( n - 1 );
            f[i+j*m] = - uxx_exact( x, y ) - uyy_exact( x, y ) + alpha * u_exact( x, y );
        }
    }

    f_norm = 0.0;
    # pragma omp parallel for private(i,j) shared(m,n,f) reduction( + : f_norm )
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            f_norm = f_norm + f[i+j*m] * f[i+j*m];
        }
    }
    f_norm = sqrt( f_norm );

    printf( "\n" );
    printf( "  Right hand side L2 norm = %f\n", f_norm );

    return f;
}






/******************************************************************************/
/******************************************************************************/
/*
Purpose:

TIMESTAMP prints the current YMDHMS date as a time stamp.

Example:

31 May 2001 09:45:54 AM

Modified:

24 September 2003

Author:

John Burkardt

Parameters:

None
*/
/******************************************************************************/
/******************************************************************************/
void timestamp( void )
{
    # define TIME_SIZE 40

    static char time_buffer[TIME_SIZE];
    const struct tm *tm;
    size_t len;
    time_t now;

    now = time( NULL );
    tm = localtime( &now );

    len = strftime( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );

    printf( "%s\n", time_buffer );

    return;
    # undef TIME_SIZE
}




/******************************************************************************/
/******************************************************************************/
/*
Purpose:

Calculate the current time (Wall Time)
*/
/******************************************************************************/
/******************************************************************************/
void getWallTime(struct timespec *ts) {
    #ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
        clock_serv_t cclock;
        mach_timespec_t mts;
        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
        clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);
        ts->tv_sec = mts.tv_sec;
        ts->tv_nsec = mts.tv_nsec;
    #else
        clock_gettime(CLOCK_REALTIME, ts);
    #endif
}




/******************************************************************************/
/******************************************************************************/
/*
Purpose:

Subtract two times
*/
/******************************************************************************/
/******************************************************************************/
struct timespec diffBetweenStartAndEnd(struct timespec *start, struct timespec *end) {
    struct timespec diff;
    diff.tv_sec = end->tv_sec - start->tv_sec;
    if( end->tv_nsec >= start->tv_nsec ) {
        diff.tv_nsec = end->tv_nsec - start->tv_nsec;
    }
    else {
        diff.tv_sec--;
        diff.tv_nsec = end->tv_nsec + 1000000000L - start->tv_nsec;
    }
    return diff;
}




/******************************************************************************/
/******************************************************************************/
/*
Purpose:

U_EXACT returns the exact value of U(X,Y).

Modified:

21 November 2007

Author:

John Burkardt

Parameters:

Input, double X, Y, the point at which the values are needed.

Output, double U_EXACT, the value of the exact solution.
*/
/******************************************************************************/
/******************************************************************************/
double u_exact( double x, double y )
{
    double value;

    value =( 1.0 - x * x ) * ( 1.0 - y * y );

    return value;
}





/******************************************************************************/
/******************************************************************************/
/*
Purpose:

UXX_EXACT returns the exact second X derivative of the solution.

Modified:

21 November 2007

Author:

John Burkardt

Parameters:

Input, double X, Y, the point at which the values are needed.

Output, double UXX_EXACT, the exact second X derivative.
*/
/******************************************************************************/
/******************************************************************************/
double uxx_exact( double x, double y )
{
    double value;

    value = -2.0 * ( 1.0 + y ) * ( 1.0 - y );

    return value;
}






/******************************************************************************/
/******************************************************************************/
/*
Purpose:

UYY_EXACT returns the exact second Y derivative of the solution.

Modified:

21 November 2007

Author:

John Burkardt

Parameters:

Input, double X, Y, the point at which the values are needed.

Output, double UYY_EXACT, the exact second Y derivative.
*/
/******************************************************************************/
/******************************************************************************/
double uyy_exact( double x, double y )
{
    double value;

    value = -2.0 * ( 1.0 + x ) * ( 1.0 - x );

    return value;
}

