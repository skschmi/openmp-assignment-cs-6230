//***********************************
//**  CODE SNIPPET - ALGORITHM 1   **
//***********************************

int this_thread;
int num_threads;
double error_norm_local;

#pragma omp parallel private(it,i,j,this_thread,num_threads,first_part,error,error_norm_local) shared(itcount,error_norm,f,n,m,u,u_old,ax,ay,b,dx,dy,it_max,error_norm_old,num_old_errors)
{
    this_thread = omp_get_thread_num();
    num_threads = omp_get_num_threads();

    //Timestep loop
    for( it = 1; it <= it_max; it++ ) {
    
        #pragma omp barrier
    
        error_norm_local = 0.0;
        
        if(this_thread == 0) {
            error_norm_old = error_norm;
            error_norm = 0.0;
            itcount++;
        }
    
        //Copy new solution into old.
        for(j = this_thread; j < n; j=j+num_threads) {
            for( i = 0; i < m; i++ ) {
                u_old[i+m*j] = u[i+m*j];
            }
        }
        
        #pragma omp barrier
        
        //Compute stencil
        for(j = this_thread; j < n; j=j+num_threads) {
            for( i = 0; i < m; i++ ) {
            
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u_old[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                               + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                               + b  *  u_old[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                }

                //Update the solution.
                u[i+j*m] = u_old[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm_local += (error*error);

            }
        }
    
        #pragma omp barrier
    
        #pragma omp critical
        error_norm += error_norm_local;
        
        #pragma omp barrier
        
        if(this_thread == 0) {

            //Error check.
            error_norm = sqrt(error_norm) / (double)(m*n);
            if( num_old_errors < 2 ) num_old_errors++;

            if(it % 100 == 0) {
                printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            }
        }
        
        #pragma omp barrier
        
        //Break
        if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
            if(this_thread == 0) printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
            break;
        }
        
    }
}