//***********************************
//**  CODE SNIPPET - ALGORITHM 3   **
//***********************************

int this_thread;
int num_threads;
int width;
int diag;

for( it = 1; it <= it_max; it++ ) {

    error_norm_old = error_norm;
    error_norm = 0.0;

    //Compute stencil, residual, and update.
    # pragma omp parallel private(i,j,k,width,error,first_part,this_thread,num_threads,diag) shared( m, n, ax, ay, b, f, omega, u ) reduction( + : error_norm )
    {
        this_thread = omp_get_thread_num();
        num_threads = omp_get_num_threads();
    
        width = 0;
        
        for( k = 0; k < (2*m)-1; k++ ) {            
            
            //Set the width of the wavefront
            if(k < m) {
                width++;
            }
            else {
                width--;
            }
            
            for(diag = this_thread; diag < width; diag = diag + num_threads) {
            
                i = diag;
                j = width - diag - 1;
            
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u[i-1+j*m]   + u[i+1+j*m]) 
                               + ay * (u[i+(j-1)*m] + u[i+(j+1)*m]) 
                               + b  *  u[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                }

                //Update the solution.
                u[i+j*m] = u[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm = error_norm + (error*error);
            
            }
            
            #pragma omp barrier

        }
    
    }

    //Error check.
    error_norm = sqrt(error_norm) / (double)(m*n);
    if( num_old_errors < 2 ) num_old_errors++;

    //Break or print
    if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        break;
    }
    else if(it % 100 == 0) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
    }

}