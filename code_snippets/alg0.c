//***********************************
//**  CODE SNIPPET - ALGORITHM 0   **
//***********************************

for( it = 1; it <= it_max; it++ ) {

    error_norm_old = error_norm;
    error_norm = 0.0;

    //Copy new solution into old.
    # pragma omp parallel for private(i,j) shared(n,m,u,u_old)
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            u_old[i+m*j] = u[i+m*j];
        }
    }

    //Compute stencil, residual, and update.
    # pragma omp parallel for private(i,j,error,first_part) shared( m, n, ax, ay, b, f, omega, u, u_old ) reduction( + : error_norm )
    for( j = 0; j < n; j++ ) {
        for( i = 0; i < m; i++ ) {
            
            //Evaluate the residual.
            if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                error = u_old[i+j*m] - f[i+j*m];
            }
            else {
                first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                           + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                           + b  *  u_old[i+j*m]     - f[i+j*m] );
                error = first_part / b;
            }

            //Update the solution.
            u[i+j*m] = u_old[i+j*m] - omega * error;

            //Accumulate the residual error.
            error_norm = error_norm + (error*error);
            
        }
    }

    //Error check.
    error_norm = sqrt(error_norm) / (double)(m*n);
    if( num_old_errors < 2 ) num_old_errors++;

    //Break or print
    if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        break;
    }
    else if(it % 100 == 0) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
    }

}