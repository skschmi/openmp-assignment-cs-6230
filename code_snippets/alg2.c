//***********************************
//**  CODE SNIPPET - ALGORITHM 2   **
//***********************************

int this_thread;
int num_threads;
double error_norm_local;

for( it = 1; it <= it_max; it++ ) {

    error_norm_old = error_norm;
    error_norm = 0.0;
    itcount++;

    # pragma omp parallel private(this_thread,num_threads,error_norm_local,i,j,error,first_part) shared(m,n,ax,ay,b,f,omega,u,u_old) reduction( + : error_norm )
    {
        this_thread = omp_get_thread_num();
        num_threads = omp_get_num_threads();
        error_norm_local = 0.0;
    
        //Copy new solution into old.            
        for(j = this_thread; j < n; j=j+num_threads) {
            for( i = 0; i < m; i++ ) {
                u_old[i+m*j] = u[i+m*j];
            }
        }

        #pragma omp barrier

        //Compute stencil, residual, and update.
        for(j = this_thread; j < n; j=j+num_threads) {
            for( i = 0; i < m; i++ ) {
            
                //Evaluate the residual.
                if( i == 0 || i == m - 1 || j == 0 || j == n-1 ) {
                    error = u_old[i+j*m] - f[i+j*m];
                }
                else {
                    first_part = (    ax * (u_old[i-1+j*m]   + u_old[i+1+j*m]) 
                               + ay * (u_old[i+(j-1)*m] + u_old[i+(j+1)*m]) 
                               + b  *  u_old[i+j*m]     - f[i+j*m] );
                    error = first_part / b;
                }

                //Update the solution.
                u[i+j*m] = u_old[i+j*m] - omega * error;

                //Accumulate the residual error.
                error_norm_local = error_norm_local + (error*error);
            }
        }
        error_norm += error_norm_local;
        
    }

    //Error check.
    error_norm = sqrt(error_norm) / (double)(m*n);
    if( num_old_errors < 2 ) num_old_errors++;

    //Break or print
    if( num_old_errors >= 2 && error_norm_old >= error_norm && error_norm <= tol ) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
        break;
    }
    else if(it % 100 == 0) {
        printf( "  %4d  Residual RMS %2.8e\n", it, error_norm );
    }
}