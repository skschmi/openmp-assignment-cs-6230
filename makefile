# make file for OpenMP project

CC=gcc
EXE=out

$(EXE): hemholtz.c
	$(CC) -fopenmp -o $(EXE) hemholtz.c -lm

clean:
	rm -f ./$(EXE)
	rm -f ./*.o
	
serial:
	$(CC) -o $(EXE) hemholtz.c -lm